edit {{cookiecutter.slug}}.md
setlocal syntax=markdown
setlocal formatoptions=ant
setlocal columns=100
setlocal tw=80
!gawk 'match($0, /^@.*\{([^,]*)/, r) {print r[1]}' {{cookiecutter.slug}}.bib | uniq | sort > .bibkeys
setlocal dict+=.bibkeys
setlocal complete+=k
