---
author:
- {{cookiecutter.author}}
title: {{cookiecutter.paper_title.split(':')[0]}}
subtitle: {{':'.join(cookiecutter.paper_title.split(':')[1:]).strip()}}
abstract: _Abstract:_
toc: false
link-citations: true
papersize: A4
bibliography: {{cookiecutter.slug}}.bib
csl: assets/csl/{{cookiecutter.citation_style}}.csl
css: assets/css/style.css
---

# Introduction {-}

There is a lack of clarity on the origins of cocktails.[^note] Traditionally
cocktails were a mixture of spirits, sugar, water, and bitters. But by the
1860s, a cocktail frequently included a liqueur (@fig:fig1).

![A figure.](https://placehold.it/550x250?text=Fig.+1){{ '{#fig:fig1}' }}

# Section {{ '{#sec:sec1}' }}

The first publication of a bartenders' guide which included cocktail recipes
was in 1862 -- _How to Mix Drinks; or, The Bon Vivant's Companion_, by
"Professor" Jerry Thomas (@tbl:tbl1). In addition to listings of recipes for
punches, sours, slings, cobblers, shrubs, toddies, flips, and a variety of
other types of mixed drinks were 10 recipes for drinks referred to as
"cocktails." A key ingredient which differentiated cocktails from other drinks
in this compendium was the use of bitters as an ingredient [@EconomistNew
1--12]. Mixed drinks popular today that conform to this original meaning of
"cocktail" include the Old Fashioned whiskey cocktail, the Sazerac cocktail,
and the Manhattan cocktail [@DavisPlanetNLR; @KnoxNetworks; @BenderEdge]. 

| Right | Left | Default | Center |
|------:|:-----|---------|:------:|
|   12  |  12  |    12   |    12  |
|  123  |  123 |   123   |   123  |
|    1  |    1 |     1   |     1  |

: A table. {{ '{#tbl:tbl1}' }}

# References {-}

[^note]: A similar critique can be found in @AronowitzScience.
