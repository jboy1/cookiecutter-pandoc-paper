import subprocess
try:
    from urllib import urlretrieve
except ImportError:
    from urllib.request import urlretrieve


try:
    urlretrieve('{{cookiecutter.bib_url}}', '{{cookiecutter.slug}}.bib')
except:
    print('Could not download bibliography file.')
try:
    urlretrieve('https://rawgit.com/citation-style-language/styles/master/{}.csl'.format('{{cookiecutter.citation_style}}'),
            'assets/csl/{{cookiecutter.citation_style}}.csl')
except:
    print('Could not download citation style file.')


subprocess.call(['git', 'init'])
subprocess.call(['git', 'add', '*'])
subprocess.call(['git', 'commit', '-m', 'Initial commit.'])
subprocess.call(['git', 'remote', 'add', 'origin', '{{cookiecutter.git_remote}}'])
